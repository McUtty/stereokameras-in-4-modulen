import numpy as np
import cv2
from matplotlib import pyplot as plt

imageToDisp = './scenes/photo.png'
photo_width = 640
photo_height = 240
image_height = 240
image_width = 320
image_size = (image_width, image_height)

pair_img = cv2.imread(imageToDisp, 0)
# Read image and split it in a stereo pair
print('Read and split image...')
imgL = pair_img[0:photo_height, 0:image_width]  # Y+H and X+W
#grayL = cv2.cvtColor(imgL, cv2.COLOR_BGR2GRAY)
imgR = pair_img[0:photo_height, image_width:photo_width]  # Y+H and X+W
#grayR = cv2.cvtColor(imgR, cv2.COLOR_BGR2GRAY)
cv2.imwrite('./scenes/photoL.png', imgL)
cv2.imwrite('./scenes/photoR.png', imgR)

stereo = cv2.createStereoBM(numDisparities=16, blockSize=15)
disparity = stereo.compute(imgL, imgR)
plt.imshow(disparity, 'gray')
plt.show()
